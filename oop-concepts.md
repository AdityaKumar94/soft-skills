# OOP Concepts in software development.
 
The object is a real-world entity that has both properties(states) and behaviors(methods). For ex., a book item can have properties like title, author, price, publishers, and a book item can have methods like getbookTitle, etc. In our software system, there can be many entities that can be an object. Let us take the example of e-commerce sites. There are entities like shopping cart, customer, product, order, order item, etc. That can be treated as an object. A customer object can have properties like username, email, password, address, phone number, etc., and different methods to retrieve and set those properties. 

So object-oriented analysis focuses on designing a system by treating every entity as an object, finding the relationship between objects, making the interface for each object, and writing code. UML(Unified Modeling Language ) is a tool used by developers to perform object-oriented analysis.

#### Following is a brief description of different OOP concepts.

* Objects: Objects represent a real-world entity that has properties and methods. An object can also contain some other object. For example, an library management system can have an object like a book, a student, etc.

* Class: Class is a blueprint of objects. The class decides the nature and behavior of an object.

* Encapsulation: Encapsulation is a way of binding data and methods that operate on that data to a single unit. We provide methods to access those data.

* Abstraction: Abstraction is just an extension of encapsulation. Abstraction exposes only relevant methods to end-user by hiding the internal implementation. In java, we have the concept of abstract class and interfaces where we declare methods. The class who implement those abstract class and interface override the methods.

* Inheritance: Inheritance is a mechanism by which a child object acquires all properties and behavior from the parent object. Parent and child class has Is-a relationship between them. For ex. a dog is an animal, dog class inherits all properties and behavior of an animal class.

* Polymorphism: Polymorphism means the ability to take a different form. In inheritance, a method in child class exists in other forms than that of in parents. Method overriding is called run time polymorphism. Method overloading is called compile-time polymorphism.


#### Classes and Objects
<br>
Class is a blueprint of an object. Class represent all common properties and methods which will be available in all instance of this class. A class has the following components.

I). Access modifiers: Access modifiers specifies the availability or accessibility of class, methods, constructor, or fields. There is 4 type of access modifiers in java. Those are following [[1]](#1).
1. Private: Access level is only within the class.
2. Default: Access level is only within the package.
3. Protected: Access level is only within the package. Also can be accessed from other packages.
4. Public: This is accessible from any package.

II). Class Keyword: In Java class is created using the class keyword.

III).ClassName: Classes are named using the PascalNaming convention in which the first letter of each word is in upper case. Methods named as per came naming convention in which the first letter of the first word is lower rest as per pascal convention.

IV). Class body: All content of the class is inside the curly bracket {}.

The object is an instance of the class. It has fields and behavior(methods).
<br>


```java
class Triangle{
    private int a;
    private int b;
    private int c;

    //Constructor without argument
    public Triangle(){
        this(0,0,0);      //The first constructor calls 2nd constructor.
    }

    //2nd constructor
    public Triangle(int a, int b, int c){
        this.a=a;
        this.b=b;
        this.c=c;
    }
}
//Initialization.
Triangle firstTriangle = new Triangle();

Triangle secondtriangle = new Triangle(2,7,9);

```
#### Encapsulation

Encapsulation is a mechanism of hiding data. So data can not be manipulated by the end-user. The Player class points field can be initialized only with a value between zero to hundred. [[2]](#2).

```java
public class Player{
    private String name;
    private int points=100; //Initially points=500
    
    public Player(String name, int points){
        this.name=name;
        if(point>0 && point<=100){
            this.points=points;
        }
    }

    public void losePoint(int lostPoint){
        this.points=this.points-lostPoint;
        if(this.points<=0){
            System.out.println("Knocked out");
        }
    }
    public int getPoints(){
        return this.points;
    }
}
```

#### Abstraction
Abstraction is a way to hide the internal implementation of any function/method. In java, there is two way to achieve abstraction [[3]](#3).
 1. Abstract class
 2. Interface 
Abstraction by abstract class.
```java

public abstract class Animal{
    private String name;

    public Animal(String name){
        this.name=name;
    }
    public abstract void eat();

    public String getName(){
        return this.name;
    }
}

public class Cow extends Animal{
    public Cow(String name){
        super(name);
    }

    @override
    public void eat(){
      System.out.println(getName()+" is eating grass");
    }
}

public class Lion extends Animal{
    public Lion(String name){
        super(name);
    }

    @override
    public void eat(){
        System.out.println(getName()+"  is eating meat");
    }
}

```

In the above example, Animal class has the abstract method eat()  that is overridden by Cow and Lion differently.
Abstraction by Interface
We use an interface when more than one class has the same functionality. So instead of overriding, we override at one place

```java
public abstract class Animal{
    private String name;

    public Animal(String name){
        this.name=name;
    }
    public abstract void eat();

    public String getName(){
        return this.name;
    }
}
public interface EatGrass{
    void eatGrass();
}
public abstract class Herbivore extends Animal implements EatGrass {
    public Herbivore(String name){
        super(name);
    }
    
    @override
    public void eatGrass(){
        System.out.println(getName()+" is eating grass");
    }
}

public class Cow extends Herbivore{
    public Cow(String name){
        super(name);
    }
    //Herbivore class implements interface EatGrass we don't need override method here
}

public class Deer extends Herbivore{
    public Deer(String name){
        super(name);
    }
}

```

#### Inheritance
Inheritance is a mechanism by which a child class inherits from the parent class. Types of inheritance.
1. Single inheritance: When a child inherits from a single parent.
2. Multiple inheritances: When a child inherits from more than two-parent.
3. Multi-Level inheritance: When a child has a grandparent in a linear fashion
4. Hierarchical inheritance: When two child inherits from the same parent.
5. Hybrid inheritance: when inheritance is a mix of more than one above type[[4]](#4).

```java
public class Animal{
    private String name;
    private int weight;
    private int legs;

    publiuc Animal(String name, int weight, int legs){
        this.name=name;
        this.weight=weight;
        this.legs=legs;
    }

    public void voice(){
        System.out.println(name+"is speaking");
    }
}

public class Lion extends Animal{
    public Lion(String name , int weight, int legs){
        super(name, weight, legs);
    }

    @override
    public void voice(){
        System.out.println(name+" is roaring");
    }
}
```

#### Polymorphism
Polymorphism is the property of an object/method to existing in two or more forms. In the inheritance method in the parent class and child have different implementations. Method overloading is another form of polymorphism in which two functions in the same class have a different number of arguments. [[5]](#5).

```java
public class Dog{
    public void bark(){
        system.out.out("woof");
    }
    public void bark(int number){
        for(int i=0;i<number;i++){
            System.out.println("woof");
        }
    }
}
```

#### References
<a id="1">[1]</a> 
 https://www.geeksforgeeks.org/access-modifiers-java/
 <br>
 <a id="2">[2]</a> 
 https://www.w3schools.com/java/java_encapsulation.asp
 <br>
  <a id="3">[3]</a>
 https://www.geeksforgeeks.org/abstraction-in-java-2/
 <br>
  <a id="4">[4]</a>
  Java The Complete Reference. Eleventh Edition.By Herbert Schildt
  Part-1, Chapter 8.
  <br>
  <a id="5">[5]</a>
  https://docs.oracle.com/javase/tutorial/java/IandI/polymorphism.html





